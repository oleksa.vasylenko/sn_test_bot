# Social Network API Test Bot
Simple bot to demonstrate API's abilities

# How to install 
1. Clone repo
2. Create virtual environment (e.g. `mkvirtualenv --python=$(which python3) test_api`)
3. Install dependencies`pip install -r requirements.txt`

# Hot to run
`python src/main.py`

# Configuration
You can adjust bot's amount of requests for user/post creation etc by changing 
parameters in `settings/config.yml`