from abc import ABCMeta, abstractmethod
import yaml
import json
import requests
import collections
import logging
import random

User = collections.namedtuple('User', 'username password email token')


class AbstractTestBot(metaclass=ABCMeta):
    @abstractmethod
    def register_users(self):
        pass


class AbstractConfigLoader:
    @abstractmethod
    def load(self):
        pass


class YamlConfigLoader(AbstractConfigLoader):
    def __init__(self, file_path):
        self.file_path = file_path

    def load(self):
        with open(self.file_path, 'r') as file:
            return yaml.load(file)


class TestApiBot(AbstractTestBot):
    NUMBER_OF_USERS = 'number_of_users'
    MAX_POSTS_PER_USER = 'max_posts_per_user'
    MAX_LIKES_PER_USER = 'max_likes_per_user'

    SIGN_UP_URL = 'auth/sign_up'
    GET_TOKEN_URL = 'auth/token'
    POSTS_URL = 'posts'
    MAKE_POST_URL = 'make'
    LIKE_URL = 'like'

    def __init__(self, api_url, config):
        self._config = config
        self._api_url = api_url
        self._users = []
        self._post_ids = []

    def register_users(self):
        logging.info("Registering users...")
        for i in range(self._config.get(self.NUMBER_OF_USERS, 0)):
            data = {
                'username': f'api-test-user-{i}',
                'password': 'test_password_123',
                'email': f'{i}_user@example.com'
            }

            res = requests.post(f'{self._api_url}/{self.SIGN_UP_URL}/', json=data)

            if res.status_code == 201:
                token = self._get_token(data)
                if token:
                    user = User(**data, token=token)
                    self._users.append(user)
            else:
                logging.warning(res.text)
        logging.info(f'Done. We have {len(self._users)} registered users')

    def _get_token(self, form_data):
        res = requests.post(f'{self._api_url}/{self.GET_TOKEN_URL}/', json=form_data)
        if res.status_code == 200:
            token = json.loads(res.content).get('access')
            return token
        logging.warning(res.text)

    def make_posts(self):
        if not self._users:
            logging.warning('No users to make posts')
            return
        logging.info('Making posts...')
        for user in self._users:
            for i in range(random.randint(0, self._config.get(self.MAX_POSTS_PER_USER, 1))):
                url = f'{self._api_url}/{self.POSTS_URL}/{self.MAKE_POST_URL}/'
                post = {
                    'title': f'Test Post {i} by {user.username}',
                    'body': f'Some dummy text'
                }
                headers = {'Authorization': f'Bearer {user.token}'}
                res = requests.post(url, json=post, headers=headers)

                if res.status_code == 201:
                    res_data = json.loads(res.content)
                    post_id = res_data.get('id')
                    if post_id:
                        self._post_ids.append(post_id)
                        logging.info(f'Made post {post_id} by {user.username}')
                else:
                    logging.warning(res.text)
        logging.info(f'Finished making posts! Total posts made: {len(self._post_ids)}')

    def give_likes(self):
        if not self._users:
            logging.warning('No users to give likes')
            return
        if not self._post_ids:
            logging.warning('No posts to give likes')
            return
        logging.info('Giving likes...')
        cnt = 0
        for user in self._users:
            for i in range(random.randint(0, self._config.get(self.MAX_LIKES_PER_USER, 1))):
                post_id = random.choice(self._post_ids)
                url = f'{self._api_url}/{self.POSTS_URL}/{post_id}/{self.LIKE_URL}/'
                headers = {'Authorization': f'Bearer {user.token}'}
                res = requests.post(url, headers=headers)

                if res.status_code == 200:
                    logging.info(f'Post {post_id} {res.text} by {user.username}')
                    cnt += 1
                else:
                    logging.warning(res.text)
        logging.info(f'Finished giving likes! Total (un)likes given: {cnt}')


def main():
    config = YamlConfigLoader('../settings/config.yaml').load()
    bot = TestApiBot(api_url='http://127.0.0.1:8000',
                     config=config)
    bot.register_users()
    bot.make_posts()
    bot.give_likes()


if __name__ == '__main__':
    logging.basicConfig(level='INFO', format="%(asctime)s %(levelname)-8s %(message)s")
    main()
